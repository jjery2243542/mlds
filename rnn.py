import theano
import numpy as np
import theano.tensor as T
from itertools import izip
from mkseq import mkseq

DIM_INPUT = 48
DIM_HIDDEN = 128
DIM_OUTPUT = 48
LOAD_PARAMS = False

def sigmoid(z):
    return 1 / (1 + T.exp(-z))

def softmax(a):
    return T.exp(a) / T.sum(T.exp(a))

def step(x_t, a_tm1, y_tm1):
    a_t = sigmoid(T.dot(Wi, x_t) + T.dot(Wh, a_tm1) + Bh)
    y_t = softmax(T.dot(Wo, a_t) + Bo)
    return a_t, y_t

def Update(params, gradients):
    mu = np.float32(0.001)
    params_updates = [(p, p - mu * g) for p, g in izip(params, gradients)]
    return params_updates

def load_params(params): 
    params[0].set_value(np.loadtxt("./params/rnn_Wi"))
    params[1].set_value(np.loadtxt("./params/rnn_Wh"))
    params[2].set_value(np.loadtxt("./params/rnn_Wo"))
    params[3].set_value(np.loadtxt("./params/rnn_Bh"))
    params[4].set_value(np.loadtxt("./params/rnn_Bo"))

def write_params(params):
    np.savetxt("./params/rnn_Wi", params[0].get_value())
    np.savetxt("./params/rnn_Wh", params[1].get_value())
    np.savetxt("./params/rnn_Wo", params[2].get_value())
    np.savetxt("./params/rnn_Bh", params[3].get_value())
    np.savetxt("./params/rnn_Bo", params[4].get_value())
        
#init
Wi = theano.shared(np.random.uniform(-0.1, 0.1, (DIM_HIDDEN, DIM_INPUT)))
Wh = theano.shared(np.random.uniform(-0.1, 0.1, (DIM_HIDDEN, DIM_HIDDEN)))
Wo = theano.shared(np.random.uniform(-0.1, 0.1, (DIM_OUTPUT, DIM_HIDDEN)))
Bh = theano.shared(np.zeros(DIM_HIDDEN))
Bo = theano.shared(np.zeros(DIM_OUTPUT))
params = [Wi, Wh, Wo, Bh, Bo]
if LOAD_PARAMS:
    load_params(params)
a_0 = theano.shared(np.zeros(DIM_HIDDEN))
y_0 = theano.shared(np.zeros(DIM_OUTPUT))
x_seq = T.matrix("input")
y_hat = T.matrix("answer")

#define functions
[a_seq, y_seq],_ = theano.scan(step, sequences=x_seq, outputs_info=[a_0, y_0], truncate_gradient=-1)
cost = T.sum((y_seq - y_hat) ** 2)
gradients = T.grad(cost, params)

rnn_train = theano.function(inputs=[x_seq, y_hat], outputs=cost, updates=Update(params, gradients))

#get data
x_seq_maker = mkseq("./train_prob_sort.ark")
x_seq_list = x_seq_maker.get_id_seq()
y_hat_maker = mkseq("./label/train_sort_lab_to_feat.ark")
y_hat_seq_list = y_hat_maker.get_id_seq()
#train
for epoch in range(3):
    a_0.set_value(np.zeros(DIM_HIDDEN))
    for idx in range(len(x_seq_list)):
        print "iter:", epoch, "seq:", idx, "average cost:", rnn_train(np.array(x_seq_list[idx]), np.array(y_hat_seq_list[idx])) / len(x_seq_list[idx])

#write params to files
write_params(params)
